#!/bin/sh
#
# There are still issues with discrete GPUs, but it works on desktops. You can replicate the same steps to install Resolve on Fedora 39 bare metal.
# Also requires a GPU of some variety. Have not tested Intel ARC, but AMD and Nvidia work.
# I used Fedora 38, but Fedora 39 currently spits out errors with Nvidia. No issues on bare metal or VMs.
#
# Install RPM Fusion if not already installed
# Check for RPM Fusion non-free
trafotinos_rpmfusion_nonfree=/etc/yum.repos.d/rpmfusion-nonfree.repo
[ -z "$trafotinos_rpmfusion_nonfree" ] && sudo dnf in -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-"$(rpm -E %fedora)".noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-"$(rpm -E %fedora)".noarch.rpm

#Installs DaVinci Resolve (Studio)'s dependencies for Fedora 38 in distrobox.
# rocm-opencl is only needed if you are an AMD GPU user.
sudo dnf in -y fuse fuse-libs alsa-lib alsa-plugins-pulseaudio apr apr-util dbus-libs fontconfig freetype libglvnd libglvnd-egl libglvnd-glx libICE librsvg2 libSM libX11 libxcrypt-compat libXcursor libXext libXfixes libXi libXinerama libxkbcommon libxkbcommon-x11 libXrandr libXrender libXtst libXxf86vm mesa-libGLU mtdev pulseaudio-libs rocm-opencl xcb-util xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm xdg-utils

# You need to install Nvidia whether you need it or not. Right now, it's just broken.
sudo dnf in -y akmod-nvidia xorg-x11-drv-nvidia-cuda

# Alert user to install their version of DaVinci.
echo "Please download your desired version of DaVinci from their website. You will be redirected in 5 seconds. Once you have unpacked and run the installer. Once the installer is complete, run script 2."
sleep 5
echo "Opening Black Magic's website..."
xdg-open https://www.blackmagicdesign.com/products/davinciresolve
