#!/bin/sh
#
# You must install Part 1.
# Replace most of Resolve's outdated libraries
sudo cp /lib64/libglib-2.0.so.0* /opt/resolve/libs

# You must install an old library from Fedora 38 because Resolve relies on it.
# Tested on Fedora, Void Linux, and SUSE: https://old.reddit.com/r/voidlinux/comments/12g71x0/davinci_resolve_18_symbol_lookup_error_libgdk/ju9ygqx/
# Archive.org mirror: https://web.archive.org/web/20231220041143if_/https://dl.fedoraproject.org/pub/fedora/linux/releases/38/Everything/x86_64/os/Packages/g/gdk-pixbuf2-2.42.10-2.fc38.x86_64.rpm
# If you have installed Resolve prior to this change in Fedora, you don't need to do anything.
sudo dnf install cpio -y
wget https://dl.fedoraproject.org/pub/fedora/linux/releases/38/Everything/x86_64/os/Packages/g/gdk-pixbuf2-2.42.10-2.fc38.x86_64.rpm 
rpm2cpio ./gdk-pixbuf2-2.42.10-2.fc38.x86_64.rpm | cpio -idmv
sudo cp -r usr/lib64/* /opt/resolve/libs
# Clean up packages
rm -r usr
rm gdk-pixbuf2-2.42.10-2.fc38.x86_64.rpm

# Export desktop launcher to host OS
distrobox-export -a "DaVinci Resolve"

# Fixing issues related to OUI vendor scanning
echo "If you use Studio, the license checks rely on running 127.0.0.1 calls to check your network OUIs as a unique identifier. My NetworkManager configuration file breaks this and if you randomize MAC addresses, this breaks Resolve's license checking. The following must be set on your host OS."
trafotinos_davinci_network="$(test -f /etc/NetworkManager/conf.d/00-macrandomize.conf)"
#if [ -f "$trafotinos_davinci_network" ]; then
	echo "sudo sed -i 's,[connection],#[connection],g' /etc/NetworkManager/conf.d/00-macrandomize.conf"
	echo "sudo sed -i 's,ethernet.cloned-mac-address=random,#ethernet.cloned-mac-address=random,g' /etc/NetworkManager/conf.d/00-macrandomize.conf"
	echo "sudo sed -i 's,wifi.cloned-mac-address=random,#wifi.cloned-mac-address=random,g' /etc/NetworkManager/conf.d/00-macrandomize.conf"
#fi
echo "The installation is now complete."
